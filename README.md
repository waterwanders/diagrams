# diagrams

Libraries of boat related things for use with diagrams.net drawings

This is somewhat opportunistic - the things in here so far are just things
that I happened to need for https://gitlab.com/waterwanders/adrenaline

That said, if this is useful to other folks, sweet! If you wish to add some
new things to it - also awesome. I will happily accept merge requests. The
file format is an abomination though, so please reach out first to let me
know you're going to add something so I know not to also be editting it.

## 

The images used in this library have been pulled from images of the various
items on the web. No warranty is made as to their accuracy. The copyright to
the images remains with their original author, but we believe using them in
this way constitutes fair use.

Let's be real honest here- the day this repo become so popular that Victron
or B&G care about the images ... well, maybe we can convince them to start
publishing official libraries.
